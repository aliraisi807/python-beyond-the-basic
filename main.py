""" basic package structure
        path_entry/
            my_package/
                __init__.py
to create a package first
1- create a package as root directory somewhere must be in sys.path
2- then in that directory create a file __init__.py which is called 
package init file which makes a package a module

go to direcoty
1- mkdir reader
2- to turn that directory to package we need to create __init__.py
 - mkdir reader
 - touch reader/__init__.py
 - python
 - >>> import reader
 - >>> type(reader)
    <class 'module'>
 - >>> reader.__file__
'/home/sigma/pluralsight_project/python-beyond-the-basic/reader/__init__.py'
   >>>  
   to see __init__ executed like any other module go open and write
   go to __init__ write something to print in order to see then run the package
   now restart our repl
   >>>python
   >>> import reader
    reader is being imported! 

we see that our little message printed out

now that we created a basic package and add some useful content to it.

"""