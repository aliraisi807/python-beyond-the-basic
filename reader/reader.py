import os
"""now edit the class reader that could read gzipped file"""
from reader.compressed import bzipped, gzipped

extension_map = {
    '.bz2': bzipped.opener,
    '.gz': gzipped.opener,
}


class Reader:
    def __init__(self, filename):
        extension = os.path.splitext(filename)[1]
        opener = extension_map.get(extension, open)
        
        self.filename = filename
        self.f = open(self.filename, 'rt')
    
    def close(self):
        self.f.close()

    def read(self):
        return self.f.read()


"""now start a new repl.
    below is the command on repl python.

    (base) sigma@admin-pc:~/pluralsight_project/python-beyond-the-basic$ python
    Python 3.7.11 (default, Jul 27 2021, 14:32:16) 
    [GCC 7.5.0] :: Anaconda, Inc. on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import reader.reader
    reader is being imported!  
    >>> reader.reader.__file__
    '/home/sigma/pluralsight_project/python-beyond-the-basic/reader/reader.py'
    >>> r = reader.reader.Reader('reader/reader.py')
    >>> r.read()
    'class Reader:\n    def __init__(self, filename):\n        self.filename = filename\n        
    self.f = open(self.filename, \'rt\')\n    \n    def close(self):\n        
    self.f.close()\n\n    def read(self):\n        
    return self.f.read()\n\n\n
    >>> r.close()
        one thing is ugly about the code is "reader.reader." it would be more cleaner if 
        if the reader class was at the top level reader package lets do some modification.
        now in __init__ do the folowing:
        from reader.reader import Reader

    restart the repl 

    after editing the __init__ ane writing
    from reader.reader import Reader
    and do the follow command on repl python

    (base) sigma@admin-pc:~/pluralsight_project/python-beyond-the-basic$ python
    Python 3.7.11 (default, Jul 27 2021, 14:32:16) 
    [GCC 7.5.0] :: Anaconda, Inc. on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import reader
    this time the reader is more simple
    >>> r = reader.Reader('reader/__init__.py')
    >>> r.read()
    "from reader.reader import Reader\n\n# print('reader is being imported!  ')"
    >>> r.close()    
"""
# ================adding subpackages========================================

""" 
    (base) sigma@admin-pc:~/pluralsight_project/python-beyond-the-basic$ mkdir reader/compressed
    (base) sigma@admin-pc:~/pluralsight_project/python-beyond-the-basic$ touch reader/compressed/__init__.py
"""