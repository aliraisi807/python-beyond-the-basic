def found():
    print('python found me!')
found()

"""now start repl for importing this module
>>> import path_test
    importerror: no module named 'path_test'.
    in order to find it follow 
    >>> import sys
    exclude it from searching in sys.path
    >>> sys.path.append('not_searched')
    now try to omport
    >>> import path_test
    >>> path_test.found()
    python found me!
    python path environment variable listing paths added to sys.path, uses the same format as your 
    system path variable.
    >>>
"""
